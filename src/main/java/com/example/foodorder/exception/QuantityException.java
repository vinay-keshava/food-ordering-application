package com.example.foodorder.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QuantityException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final String message;
	public QuantityException(String message) {
		super();
		this.message = message;
	}
	
}
