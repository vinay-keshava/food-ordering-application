package com.example.foodorder.exception;

import java.net.http.HttpHeaders;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.example.foodorder.dto.ResponseDto;

@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		List<String> errorDetails = new ArrayList<>();
		for (ObjectError error : ex.getBindingResult().getAllErrors()) {
			errorDetails.add(error.getDefaultMessage());
		}
		ResponseDto response = new ResponseDto(errorDetails, 400);
		return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(FoodItemNotFoundException.class)
	public ResponseEntity<Object> handleFoodItemNotFound(FoodItemNotFoundException ex, WebRequest req) {
		List<String> errors = new ArrayList<>();
		errors.add(ex.getLocalizedMessage());
		ResponseDto response = new ResponseDto(errors, 404);
		return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(UserNotFoundException.class)
	public ResponseEntity<Object> handleUserNotFound(UserNotFoundException ex, WebRequest req) {
		List<String> errors = new ArrayList<>();
		errors.add(ex.getLocalizedMessage());
		ResponseDto response = new ResponseDto(errors, 404);
		return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(QuantityException.class)
	public ResponseEntity<Object> handleQuantityExceptionl(QuantityException ex, WebRequest req) {
		List<String> errors = new ArrayList<>();
		errors.add(ex.getLocalizedMessage());
		ResponseDto response = new ResponseDto(errors, 404);
		return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(value = NoSuchResultsFoundException.class)
	public ResponseEntity<Object> exception(NoSuchResultsFoundException exception){
		return new ResponseEntity<>("No such Results found", HttpStatus.NOT_FOUND);
	}
		@ExceptionHandler(value=IllegalArgumentException.class)
	public ResponseEntity<Object> exception(IllegalArgumentException exception){
		return new ResponseEntity<>("Invalid Period",HttpStatus.NOT_FOUND);
	}

}
