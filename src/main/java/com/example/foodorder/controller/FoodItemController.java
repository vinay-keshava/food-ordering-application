package com.example.foodorder.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.foodorder.dto.FoodItemDto;
import com.example.foodorder.dto.ResponseDto;
import com.example.foodorder.service.impl.FoodItemServiceImpl;

import io.swagger.v3.oas.annotations.Operation;

@RestController
@RequestMapping("/api")
public class FoodItemController {

	@Autowired
	FoodItemServiceImpl foodItemServiceImpl;
	
	@Operation(summary = "new food item")
	@PostMapping("/fooditem")
	public ResponseEntity<ResponseDto> newFoodItem(FoodItemDto foodItemDto){
		return new ResponseEntity<>(foodItemServiceImpl.newFoodItem(foodItemDto),HttpStatus.CREATED);
	}

	@Operation(summary = "search food item")
	@GetMapping("/fooditems")
	public ResponseEntity<List<FoodItemDto>> searchFood(@RequestParam String searchInput) {
		List<FoodItemDto> matchingFoodItems = foodItemServiceImpl.searchFoodItems(searchInput);

		return ResponseEntity.ok(matchingFoodItems);
	}
}
