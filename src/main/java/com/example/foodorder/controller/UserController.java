package com.example.foodorder.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.foodorder.dto.ResponseDto;
import com.example.foodorder.dto.UserDto;
import com.example.foodorder.service.impl.UserServiceImpl;

import io.swagger.v3.oas.annotations.Operation;

@RestController
@RequestMapping("/api")
public class UserController {
	
	@Autowired
	UserServiceImpl userServiceImpl;

	
	@Operation(summary = "new user")
	@PostMapping("/users")
	public ResponseEntity<ResponseDto> newUserRegistration(UserDto userDto){
		return new ResponseEntity<>(userServiceImpl.newUserRegistration(userDto),HttpStatus.CREATED);
	}
}
