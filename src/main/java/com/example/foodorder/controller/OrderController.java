package com.example.foodorder.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.foodorder.dto.OrdersDto;
import com.example.foodorder.dto.PurchaseDto;
import com.example.foodorder.dto.ResponseDto;
import com.example.foodorder.service.impl.OrdersServiceImpl;

import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api")
public class OrderController {
	
	@Autowired
	OrdersServiceImpl ordersServiceImpl;

	@Operation(summary = "new order")
	@PostMapping("/orders")
	public ResponseEntity<ResponseDto> newOrders(@Valid @RequestBody OrdersDto ordersDto){
		return new ResponseEntity<>(ordersServiceImpl.newOrder(ordersDto),HttpStatus.CREATED);
	}

	@Operation(summary = "get orders")
	@GetMapping("/orders/{userId}")
	 public ResponseEntity<List<PurchaseDto>> purchaseHistory(@RequestParam String period, @PathVariable long userId) {
	     List<PurchaseDto> purchaseHistory = ordersServiceImpl.purchaseHistory(period, userId);
	     return new ResponseEntity<>(purchaseHistory, HttpStatus.OK);
	 }

	
	
}
