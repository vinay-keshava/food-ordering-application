package com.example.foodorder.security;

import java.util.Optional;

import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.example.foodorder.entity.User;
import com.example.foodorder.repository.UserRepository;

public class UserInfoDetailsService implements  UserDetailsService{
	
	@Autowired
	UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		  Optional<User> user = userRepository.findByuserName(username);
	        return user.map(UserInfoDetails::new)
	                .orElseThrow(() -> new UsernameNotFoundException("user not found " + username));

	    
	}

	
}
