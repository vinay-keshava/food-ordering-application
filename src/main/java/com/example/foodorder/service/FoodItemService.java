package com.example.foodorder.service;

import java.util.List;

import com.example.foodorder.dto.FoodItemDto;
import com.example.foodorder.dto.ResponseDto;

public interface FoodItemService {

	List<FoodItemDto> searchFoodItems(String searchInput);

	ResponseDto newFoodItem(FoodItemDto foodItemDto);

}
