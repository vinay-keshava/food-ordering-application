package com.example.foodorder.service.impl;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.example.foodorder.dto.OrdersDto;
import com.example.foodorder.dto.PurchaseDto;
import com.example.foodorder.dto.ResponseDto;
import com.example.foodorder.entity.FoodItem;
import com.example.foodorder.entity.FoodOrders;
import com.example.foodorder.entity.OrdersItem;
import com.example.foodorder.entity.User;
import com.example.foodorder.exception.FoodItemNotFoundException;
import com.example.foodorder.exception.QuantityException;
import com.example.foodorder.exception.UserNotFoundException;
import com.example.foodorder.repository.FoodItemRepository;
import com.example.foodorder.repository.FoodOrdersRepository;
import com.example.foodorder.repository.UserRepository;
import com.example.foodorder.service.FoodOrdersService;

@Service
@Transactional(isolation = Isolation.REPEATABLE_READ)
//Isolation.Serializable -> deadlocks might occur as transactions need to wait 
//Isolation.READ_COMMITTED -> Re reads might be different
public class OrdersServiceImpl implements FoodOrdersService {

	@Autowired
	FoodOrdersRepository ordersRepository;

	@Autowired
	FoodItemRepository foodItemRepository;

	@Autowired
	UserRepository userRepository;

	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public ResponseDto newOrder(OrdersDto ordersDto) {
		User user = userRepository.findByUserId(ordersDto.getUserId());
		if (Objects.isNull(user)) {
			logger.error("User not found,please register");
			throw new UserNotFoundException("User not found,please register");

		}
		List<OrdersItem> orderItems = ordersDto.getOrderItem().stream().map(orderItemDto -> {
			FoodItem foodItem = foodItemRepository.findByvendorNameIgnoreCaseAndFoodNameIgnoreCase(
					orderItemDto.getVendorName().trim(), orderItemDto.getFoodName().trim());
			if (Objects.isNull(foodItem)) {
				logger.error(orderItemDto.getFoodName() + "Not found in " + orderItemDto.getVendorName());
				throw new FoodItemNotFoundException(
						orderItemDto.getFoodName() + "Not found in " + orderItemDto.getVendorName());
			}
			if ((foodItem.getQuantity() - orderItemDto.getQuantity() < 0) || foodItem.getQuantity() <= 0) {
				logger.error("Quantity is not available, available quantity of " + foodItem.getFoodName() + " is: "
						+ foodItem.getQuantity() + "with user-name:" + ordersDto.getUserId());
				throw new QuantityException("Quantity is not available, available quantity of " + foodItem.getFoodName()
						+ " is: " + foodItem.getQuantity());
			}
			foodItem.setQuantity(foodItem.getQuantity() - orderItemDto.getQuantity());
			foodItemRepository.save(foodItem);
			OrdersItem ordersItem = new OrdersItem();
			BeanUtils.copyProperties(orderItemDto, ordersItem);
			return ordersItem;
		}).collect(Collectors.toList());	

		double sum = orderItems.stream().mapToDouble(orderItem -> {
		    Optional<FoodItem> foodItemOptional = Optional.of(foodItemRepository.findByvendorNameIgnoreCaseAndFoodNameIgnoreCase(
		        orderItem.getVendorName(), orderItem.getFoodName()));
		    if (foodItemOptional.isPresent()) {
		        FoodItem foodItem = foodItemOptional.get();
		        return foodItem.getFoodPrice() * orderItem.getQuantity();
		    } else {
		    	throw new FoodItemNotFoundException("Unable to fetch price of the item");
		    }
		}).sum();

		FoodOrders orders = new FoodOrders();
		BeanUtils.copyProperties(ordersDto, orders);
		orders.setTotalAmount(sum);
		orders.setOrderItem(orderItems);
		orders.setUser(user);
		
		ordersRepository.save(orders);
		logger.info("Order Placed Successfully for user" + orders.getUser().getUserName());
		return new ResponseDto(Collections.singletonList("Order placed successfully"), 201);

	}

	@Override
	public List<PurchaseDto> purchaseHistory(String period, long userid) {
		List<FoodOrders> orders = ordersRepository.findAllByUserWithUser(userid);
		LocalDateTime now = LocalDateTime.now();

		return orders.stream().filter(o -> {
			LocalDateTime startFilter = now.minusYears(1);
			if (period.equals("week")) {
				startFilter = now.minusDays(7);
			} else if (period.equals("month")) {
				startFilter = now.minusMonths(1);
			} else {
				throw new IllegalArgumentException();
			}
			return o.getOrderDate().isAfter(startFilter) && o.getOrderDate().isBefore(now);
		}).map(o -> {
			PurchaseDto purchaseDto = new PurchaseDto();
			purchaseDto.setUserId(o.getUser().getUserId());
			purchaseDto.setDate(o.getOrderDate());
			purchaseDto.setAmount(o.getTotalAmount());
			List<OrdersItem> orderItems = o.getOrderItem();
			purchaseDto.setOrderItem(orderItems);
			return purchaseDto;
		}).collect(Collectors.toList());
	}

}
