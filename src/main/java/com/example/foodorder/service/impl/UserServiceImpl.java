package com.example.foodorder.service.impl;

import java.util.Collections;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.foodorder.dto.ResponseDto;
import com.example.foodorder.dto.UserDto;
import com.example.foodorder.entity.User;
import com.example.foodorder.repository.UserRepository;
import com.example.foodorder.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository userRepository;

	public ResponseDto newUserRegistration(UserDto userDto) {
		User user = new User();
		BeanUtils.copyProperties(userDto, user);
		user.setPassword(new BCryptPasswordEncoder().encode(userDto.getPassword()));
		userRepository.save(user);
	
		return new ResponseDto(Collections.singletonList("User Registration successfull"), HttpStatus.CREATED.value());
	}
}
