package com.example.foodorder.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.foodorder.dto.FoodItemDto;
import com.example.foodorder.dto.ResponseDto;
import com.example.foodorder.entity.FoodItem;
import com.example.foodorder.exception.NoSuchResultsFoundException;
import com.example.foodorder.repository.FoodItemRepository;
import com.example.foodorder.service.FoodItemService;

@Service
public class FoodItemServiceImpl implements FoodItemService{

	@Autowired
	FoodItemRepository foodItemRepository;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());


	@Override
	public ResponseDto newFoodItem(FoodItemDto foodItemDto ) {
		if(Objects.isNull(foodItemRepository.findByvendorNameIgnoreCaseAndFoodNameIgnoreCase(foodItemDto.getVendorName(), foodItemDto.getFoodName()))) {
		FoodItem foodItem = new FoodItem();
		BeanUtils.copyProperties(foodItemDto, foodItem);
		foodItemRepository.save(foodItem);
		return new ResponseDto(Collections.singletonList("New food item added successfully"), 201);
		}
		else
			return new ResponseDto(Collections.singletonList("Food item already added"),409);
	}

	@Override
	public List<FoodItemDto> searchFoodItems(String searchName) {
	   List<FoodItemDto> foodItemDto = new ArrayList<>();
	   List<FoodItem> foodByName = foodItemRepository.findByFoodNameIgnoreCaseContaining(searchName);
	   List<FoodItem> foodItems = foodByName.isEmpty() ?
	    foodItemRepository.findByVendorNameIgnoreCaseContaining(searchName) : foodByName;

	   if (foodItems.isEmpty()){
	       logger.error("No Results");
	       throw new NoSuchResultsFoundException();
	   }

	   foodItems.stream()
	           .map(entity -> {
	               FoodItemDto dto = new FoodItemDto();
	               BeanUtils.copyProperties(entity, dto);
	               return dto;
	           })
	           .forEach(foodItemDto::add);

	   return foodItemDto;
	}

}
