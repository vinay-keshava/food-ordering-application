package com.example.foodorder.service;

import java.util.List;

import com.example.foodorder.dto.OrdersDto;
import com.example.foodorder.dto.PurchaseDto;
import com.example.foodorder.dto.ResponseDto;

public interface FoodOrdersService {

	List<PurchaseDto> purchaseHistory(String period, long userid);

	ResponseDto newOrder(OrdersDto ordersDto);

	


}
