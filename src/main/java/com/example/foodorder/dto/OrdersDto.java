package com.example.foodorder.dto;

import java.util.List;

import lombok.Data;

@Data
public class OrdersDto {

	private long userId;
	private List<OrderItemDto> orderItem;
}
