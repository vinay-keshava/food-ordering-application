package com.example.foodorder.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.Data;

@Data
public class UserDto {

	@NotNull(message = "User name required field")
	private String userName;

	@NotNull(message = "Contact number is a required field")
	@Pattern(regexp = "[6789][\\d]{9}", message = "Enter a valid indian contact number")
	private String contactNumber;

	@Email(message = "Please Enter a valid email")
	private String email;

	@NotNull(message = "password should not be null")
	@Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()_+\\-=\\[\\]{};':\"\\\\|,.<>\\/?]).{8,}$", message = "Password must be at least 8 characters long and include at least one digit, one lowercase letter, one uppercase letter, and one special character.")
	private String password;

	@NotNull
	private String roles;
}
