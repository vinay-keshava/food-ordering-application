package com.example.foodorder.dto;

import java.time.LocalDateTime;
import java.util.List;

import com.example.foodorder.entity.OrdersItem;



public class PurchaseDto {
	
	
	 private long userId;
	 private List<OrdersItem> orderItem; 
	 private LocalDateTime date;
	 private double amount;
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public List<OrdersItem> getOrderItem() {
		return orderItem;
	}
	public void setOrderItem(List<OrdersItem> orderItem) {
		this.orderItem = orderItem;
	}
	public LocalDateTime getDate() {
		return date;
	}
	public void setDate(LocalDateTime date) {
		this.date = date;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	 
	 
	}


