package com.example.foodorder.dto;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class FoodItemDto {
	
	@NotNull(message = "food name is a mandatory field")
	private String foodName;
	
	@NotNull(message = "Enter the food price s")
	@Min(value = 1)
	private double foodPrice;
	
	@NotNull(message = "Vendor name should not be empty")
	private String vendorName;
	
	@Min(value = 1)
	private int quantity;
}
