package com.example.foodorder.dto;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class OrderItemDto {
	


	@NotNull(message = "quantity is a mandatory field")
	@Min(value = 1)
	private int quantity;
	
	@NotNull(message = "food name required field")
	private String foodName;
	
	@NotNull(message = "vendor name required field")
	private String vendorName;
}
