package com.example.foodorder.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.foodorder.entity.FoodItem;

@Repository
public interface FoodItemRepository extends JpaRepository<FoodItem, Long>{

	FoodItem findByvendorNameIgnoreCaseAndFoodNameIgnoreCase(String vendorName, String foodName);
	List<FoodItem> findByVendorNameIgnoreCaseContaining(String searchName);
	List<FoodItem> findByFoodNameIgnoreCaseContaining(String searchName);
}
