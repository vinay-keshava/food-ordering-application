package com.example.foodorder.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.foodorder.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	User findByUserNameAndContactNumber(String userName, String contactNumber);

	User findByUserId(long l);

	Optional<User> findByuserName(String username);

	

}
