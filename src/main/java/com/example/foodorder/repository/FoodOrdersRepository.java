package com.example.foodorder.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.foodorder.entity.FoodOrders;
import com.example.foodorder.entity.User;

@Repository

public interface FoodOrdersRepository extends JpaRepository<FoodOrders, Long> {

	User findByUser(long userid);

	@Query("SELECT o FROM FoodOrders o JOIN FETCH o.user WHERE o.user.id = :userId")
	List<FoodOrders> findAllByUserWithUser(@Param("userId") long userId);

}

