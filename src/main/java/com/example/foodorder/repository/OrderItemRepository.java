package com.example.foodorder.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.foodorder.entity.OrdersItem;

public interface OrderItemRepository extends JpaRepository<OrdersItem, Long>{

}
