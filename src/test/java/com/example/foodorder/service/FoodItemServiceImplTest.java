package com.example.foodorder.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.example.foodorder.dto.FoodItemDto;
import com.example.foodorder.entity.FoodItem;
import com.example.foodorder.exception.NoSuchResultsFoundException;
import com.example.foodorder.repository.FoodItemRepository;
import com.example.foodorder.service.impl.FoodItemServiceImpl;

@ExtendWith(SpringExtension.class)
public class FoodItemServiceImplTest {
	@Mock
	private FoodItemRepository foodItemRep;

	@InjectMocks
	private FoodItemServiceImpl foodItemService;

	@Test
	 void testSearchFoodItems_NoResultsFound() {

		String searchName = "InvalidSearch";
		List<FoodItem> emptyList = Collections.emptyList();
		when(foodItemRep.findByFoodNameIgnoreCaseContaining(searchName)).thenReturn(emptyList);
		when(foodItemRep.findByVendorNameIgnoreCaseContaining(searchName)).thenReturn(emptyList);

		assertThrows(NoSuchResultsFoundException.class, () -> {
			foodItemService.searchFoodItems(searchName);
		});
	}

	@Test
	 void testSearchFoodItems() {

		String searchName = "Chicken";

		FoodItem foodItem1 = new FoodItem();
		foodItem1.setFoodName("Chicken Pizza");

		FoodItem foodItem2 = new FoodItem();
		foodItem2.setFoodName("Pepper Chicken");

		List<FoodItem> foodByName = new ArrayList<>();
		foodByName.add(foodItem1);
		foodByName.add(foodItem2);

		when(foodItemRep.findByFoodNameIgnoreCaseContaining(anyString())).thenReturn(foodByName);
		List<FoodItemDto> result = foodItemService.searchFoodItems(searchName);

		assertEquals(2, result.size());
		assertEquals("Chicken Pizza", result.get(0).getFoodName());
		assertEquals("Pepper Chicken", result.get(1).getFoodName());
	}

	@Test
	 void testSearchFoodItemsByVendorName() {
		String searchName = "vendor";
		List<FoodItem> foodItems = new ArrayList<>();
		FoodItem foodItem1 = new FoodItem();
		foodItem1.setFoodName("Food1");
		foodItem1.setVendorName("Vendor1");
		foodItems.add(foodItem1);
		FoodItem foodItem2 = new FoodItem();
		foodItem2.setFoodName("Food2");
		foodItem2.setVendorName("Vendor2");
		foodItems.add(foodItem2);
		when(foodItemRep.findByVendorNameIgnoreCaseContaining(searchName)).thenReturn(foodItems);

		List<FoodItemDto> foodItemDtos = foodItemService.searchFoodItems(searchName);

		verify(foodItemRep).findByVendorNameIgnoreCaseContaining(searchName);
		assertThat(foodItemDtos).hasSize(2);
		assertThat(foodItemDtos.get(0).getFoodName()).isEqualTo(foodItem1.getFoodName());
		assertThat(foodItemDtos.get(1).getFoodName()).isEqualTo(foodItem2.getFoodName());
	}

}
