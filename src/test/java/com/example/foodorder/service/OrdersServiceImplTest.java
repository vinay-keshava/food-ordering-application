package com.example.foodorder.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.example.foodorder.dto.OrderItemDto;
import com.example.foodorder.dto.OrdersDto;
import com.example.foodorder.dto.ResponseDto;
import com.example.foodorder.entity.FoodItem;
import com.example.foodorder.entity.FoodOrders;
import com.example.foodorder.entity.User;
import com.example.foodorder.exception.FoodItemNotFoundException;
import com.example.foodorder.exception.QuantityException;
import com.example.foodorder.exception.UserNotFoundException;
import com.example.foodorder.repository.FoodItemRepository;
import com.example.foodorder.repository.FoodOrdersRepository;
import com.example.foodorder.repository.UserRepository;
import com.example.foodorder.service.impl.OrdersServiceImpl;

@ExtendWith(SpringExtension.class)
class OrdersServiceImplTest {
	@InjectMocks
	OrdersServiceImpl ordersServiceImpl;

	@Mock
	UserRepository userRepository;

	@Mock
	FoodItemRepository foodItemRepository;

	@Mock
	FoodOrdersRepository ordersRepository;

	@Test
	void newOrder_shouldThrowFoodItemNotFoundException_whenFoodItemNotFound() {

		OrdersDto ordersDto = new OrdersDto();
		OrderItemDto orderItemDto = new OrderItemDto();
		orderItemDto.setFoodName("Nonexistent food item");
		orderItemDto.setVendorName("ABC restaurant");
		ordersDto.setOrderItem(Collections.singletonList(orderItemDto));

		User user = new User();
		user.setUserId(1L);
		user.setUserName("John");
		when(userRepository.findByUserId(ordersDto.getUserId())).thenReturn(user);
		// Act and Assert
		FoodItemNotFoundException exception = assertThrows(FoodItemNotFoundException.class, () -> {
			ordersServiceImpl.newOrder(ordersDto);
		});
		assertEquals("Nonexistent food itemNot found in ABC restaurant", exception.getMessage());
	}

	@Test
	void testNewOrderWithInsufficientQuantity() {
		// Arrange
		OrdersDto ordersDto = new OrdersDto();
		ordersDto.setUserId(1L);

		List<OrderItemDto> orderItemDtoList = new ArrayList<>();
		OrderItemDto orderItemDto = new OrderItemDto();
		orderItemDto.setFoodName("Pizza");
		orderItemDto.setVendorName("Pizza Hut");
		orderItemDto.setQuantity(100); // This quantity is greater than the available quantity
		orderItemDtoList.add(orderItemDto);

		ordersDto.setOrderItem(orderItemDtoList);

		when(userRepository.findByUserId(ordersDto.getUserId())).thenReturn(new User());
		FoodItem foodItem = new FoodItem();
		foodItem.setFoodName(orderItemDto.getFoodName());
		foodItem.setVendorName(orderItemDto.getVendorName());
		foodItem.setFoodPrice(50.0);
		foodItem.setQuantity(10);

		when(foodItemRepository.findByvendorNameIgnoreCaseAndFoodNameIgnoreCase(orderItemDto.getVendorName(),
				orderItemDto.getFoodName())).thenReturn(foodItem);

		// Act and Assert
		assertThrows(QuantityException.class, () -> {
			ordersServiceImpl.newOrder(ordersDto);
		});
	}

	@Test
	void testNewOrderSuccess() {
		User user = new User();
		user.setUserId(1L);
		user.setUserName("John");

		FoodItem foodItem1 = new FoodItem();
		foodItem1.setFoodName("Burger");
		foodItem1.setVendorName("Burger King");
		foodItem1.setFoodPrice(5.0);
		foodItem1.setQuantity(10);

		FoodItem foodItem2 = new FoodItem();
		foodItem2.setFoodName("Pizza");
		foodItem2.setVendorName("Pizza Hut");
		foodItem2.setFoodPrice(10.0);
		foodItem2.setQuantity(5);

		List<OrderItemDto> orderItemDtoList = new ArrayList<>();
		OrderItemDto orderItemDto1 = new OrderItemDto();
		orderItemDto1.setFoodName("Burger");
		orderItemDto1.setVendorName("Burger King");
		orderItemDto1.setQuantity(2);
		orderItemDtoList.add(orderItemDto1);

		OrderItemDto orderItemDto2 = new OrderItemDto();
		orderItemDto2.setFoodName("Pizza");
		orderItemDto2.setVendorName("Pizza Hut");
		orderItemDto2.setQuantity(1);
		orderItemDtoList.add(orderItemDto2);

		OrdersDto ordersDto = new OrdersDto();
		ordersDto.setUserId(user.getUserId());
		ordersDto.setOrderItem(orderItemDtoList);

		// mock repository methods
		when(userRepository.findByUserId(user.getUserId())).thenReturn(user);
		when(foodItemRepository.findByvendorNameIgnoreCaseAndFoodNameIgnoreCase(foodItem1.getVendorName(),
				foodItem1.getFoodName())).thenReturn(foodItem1);
		when(foodItemRepository.findByvendorNameIgnoreCaseAndFoodNameIgnoreCase(foodItem2.getVendorName(),
				foodItem2.getFoodName())).thenReturn(foodItem2);
		when(foodItemRepository.save(any(FoodItem.class))).thenReturn(null);
		when(ordersRepository.save(any(FoodOrders.class))).thenReturn(null);

		ResponseDto responseDto = ordersServiceImpl.newOrder(ordersDto);
		assertEquals(201, responseDto.getCode());
		assertEquals(Collections.singletonList("Order placed successfully"), responseDto.getMessages());
	}

	@Test
	void testNewOrderUserNotFound() {
		OrdersDto ordersDto = new OrdersDto();
		ordersDto.setUserId(1L);
		when(userRepository.findByUserId(1L)).thenReturn(null);
		assertThrows(UserNotFoundException.class, () -> ordersServiceImpl.newOrder(ordersDto));
	}

	@Test
	void testNewOrderWithNullUser() {
		OrdersDto ordersDto = null;
		assertThrows(NullPointerException.class, () -> {
			ordersServiceImpl.newOrder(ordersDto);
		});
	}
}
