# Food Ordering Application

DATABASE: MySQL

Spring Boot Version: 3.0.2

Database Name: food_order_application

LogFile Name:foodOrderingApplication.log


## Entities

1.Food Item (foodId,foodName,foodPrice,vendorName,quantity)

2.Food Orders (orderId,OrderItem,orderDate,totalAmount)

3.Food Orders Items (orderItemId,quantity,foodName,vendorName)

4.Users (userId,userName,contactNumber)

## Controller

1. Food Item Controller
	
	a.METHOD: GET, EndPoint: Search Food Items
	
	b.METHOD: POST, EndPoint: New Food Items

2. Food Order Controller

	a.METHOD: POST, EndPoint: New Order
	
	b.METHOD: GET , EndPoint: Purchase History of a User based on week/month

3. Users Controller

	a.METHOD: POST, EndPoint: New User
